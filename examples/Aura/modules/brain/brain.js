/**
recibe modulo http desde keystone, y asocia socket.io
para manejar los eventos de clientes y el master

el cerebro es un loop asincrono que comparte sus resultados a
traves de un pozo de memoria hacia socketio, recibiendo
ordenes desde master y modificando las variables que leeran los clientes
*/
var Cookie, io, Keystone, SessionStore;

var tl = Date.now(),
	loop_function, update_brain,
	model = {
		vectors_l : 0,
		vectors: [],
		vector_interpolation: 'discrete',
	},
	users_masters = [],
	users = [];

update_brain = function update_brain(update) {
	var i, l = update.vectors_l, m = model, v, mv,
		last_user = 0

	console.log('updating model', update);

	['vector_interpolation', 'vectors_l'].map(function(p) {
		// console.log(p)
		if(update[p]) m[p] = update[p]
	});

	for(i=0; i<l; i++) {
		v = update.vectors[i]

		if(!(mv = model.vectors[i]) || v.sync) {
			mv = model.vectors[i] = v
			mv.sync = true
		}

		if(mv.sync) {
			while( ( (last_user+1)*((m.vectors_l)/users.length) )-1 <= i) {
				console.log("(m.vectors_l)/users.length",(m.vectors_l)/users.length,last_user, i)
				console.log('updating user $ on vector $', last_user, i)
				io.to(users[last_user]).emit('vector',mv)
				last_user++
			}
		}
	}
}

function loop() {
	var dt = Date.now() - tl;

	if(loop_function)
		loop_function(dt, data_pool)

	tl += dt;
}
setTimeout(loop,10)

function send_clients() {
	users.forEach(function(user) {
		console.log('sending message to ', user)
		io.to(user).emit('test', "testing comm")
	})
}

function users_log() {
	console.log('current users', users)
	console.log('current users_masters', users_masters)
}

exports = module.exports = function(keystone_local) {
	Keystone = keystone_local
	Cookie = require('cookie')
	SessionStore = Keystone.get("session options").store

	var HttpServer = Keystone.httpServer

	//keystone.list('')

	io = require('socket.io')(HttpServer)

	io.on('connection', function(sk) {
		/* Primero determinamos si el usuario esta logeado o no */
		var sessid, user;
		try {
			// del socket generado por socket-io obtenemos el header
			// , que es informacion obtenida en el handshake.
			// de los headers http obtenemos el coockie que contiene
			// informacion de la sesion, usamos Cookie.parse para
			// convertirla a un objeto, y obtenemos el this.sid, que contiene
			// el identificador de sesión mas propiedades de el,
			// que las sacamos con los split
			sessid = Cookie.parse(sk.handshake.headers.cookie)["this.sid"]
				.split(':')[1].split('.')[0]
		}
		catch(err) {
			console.log('Error processing sessión in socketio connection', err)
		}

		if(sessid) {
			SessionStore.get(sessid, function(err, data) {
				console.log('user connected')

				if(data && data.userId) {
					console.log('user loged', sk.id)
					users_masters.push(sk.id)
				}
				else {
					console.log('user not loged')
					users.push(sk.id)
				}
				send_clients()
				users_log()
			})
			sk.on('update brain', function(update) {
				update_brain(JSON.parse(update))
			})
			sk.on('disconnect', function() {
				var i
				if(!user) {
					i = users.indexOf(sk.id)
					if(i != -1)
					users.splice(i,1)
				}
				console.log('disconnected')
				users_log()
			})
		}

	} )
}
