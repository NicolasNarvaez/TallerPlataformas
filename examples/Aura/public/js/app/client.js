$(function() {

	$('#color_container').click(function(e) {
		$(e.target).toggleClass('active')
	})

	var sk = io(),
		vector_virtual, vector, vector_old, model = {
		},
		update_loop,
		color_container = $('#color_container'),
		tl = Date.now(),
		Vector = function(opts){ opts = opts || {}
			var r = opts.representation = opts.representation || {}
			r.f = r.f || 'probability'	//function de representacion
			r.h = r.h || [0,0,0,0]	//rango de incertidumbre
			r.h_speed = r.h_speed || 200	//frecuencia de cambio
			r.interpolation = r.interpolation || 'exponential'	//f interpolacion
			r.interpolation_speed = r.interpolation_speed || 150 //vel interpolacion
			return {
				//la forma de representar el vector, si directa o a
				//traves de un filtro
				representation : opts.representation,
				//la forma de interpolar el valor del vector al recibirse
				//informacion nueva del servidor, es la funcion de transicion
				interpolation : opts.interpolation || 'exponential',
				//milisegundos
				interpolation_speed : opts.interpolation_speed || 1000,
				//el vector contiene toda la informacion de estado:
				//color (rgb) y sonido (frecuencia en hz)
				// orden de componentes: r g b hz
				v: opts.v || [0,0,255,50],
				sync:true
			}
		}

	var interpolar_exponencial = function(t_start, t_end, v_start, v_end) {
		var v, t_end_offset = 0, now = Date.now()
		if(now > t_end) now = t_end
		v = (Math.exp(-2*(t_end-now)/(t_end-t_start)) - Math.exp(-2))/(Math.exp(0)-Math.exp(-2))
		return v_start + v*(v_end-v_start)
	}

	var interpolar_exponencial_vectors = function(t_start, t_end, v_start, v_end) {
		var i , l = v_start.length, v = []
		for(i=0; i<l; i++) {
			v.push( interpolar_exponencial(t_start, t_end, v_start[i], v_end[i]) )
		}
		return v
	}

	sk.on('test', function() {
		console.log('comm testing successful')
	})
	sk.on('vector', function(vec) {
		console.log('vector recibido: ',vec)
		if(vector) vector_old = vector
		vector = vec
		vec.t = Date.now()
	})

	update_loop = function(dt) {
		if(!dt) {
			dt = Date.now() - tl
			update_loop(dt)
			tl += dt
			return
		}

		if(!vector) return
		vector_virtual = Vector(vector)
		//vlcular valor actual del vector
		if(vector.interpolation == 'exponential' && vector_old) {
			vector_virtual.v = interpolar_exponencial_vectors(vector_old.t,
				vector_old.t+vector.interpolation_speed,
				vector.v, vector_old.v)
		}

		console.log('vector_virtual',vector.v, vector_virtual.v)
		color_container.css({backgroundColor:'rgb('+
			(vector.v[0])+','+
			(vector.v[1])+','+
			(vector.v[2])+')'})

	}

	setInterval(update_loop, 50)
})
