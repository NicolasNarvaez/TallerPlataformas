/**
*/
(function(){
var $ = jQuery

$(function() {
	var Vector = function(opts){ opts = opts || {}
			var r = opts.representation = opts.representation || {}
			r.f = r.f || 'probability'	//function de representacion
			r.h = r.h || [0,0,0,0]	//rango de incertidumbre
			r.h_speed = r.h_speed || 200	//frecuencia de cambio
			r.interpolation = r.interpolation || 'exponential'	//f interpolacion
			r.interpolation_speed = r.interpolation_speed || 150 //vel interpolacion
			return {
				//la forma de representar el vector, si directa o a
				//traves de un filtro
				representation : opts.representation,
				//la forma de interpolar el valor del vector al recibirse
				//informacion nueva del servidor, es la funcion de transicion
				interpolation : opts.interpolation || 'exponential',
				//milisegundos
				interpolation_speed : opts.interpolation_speed || 1000,
				//el vector contiene toda la informacion de estado:
				//color (rgb) y sonido (frecuencia en hz)
				// orden de componentes: r g b hz
				v: opts.v || [0,0,255,50],
				sync:true
			}
		},
		model = {
			vectors_l: 3,
			vectors: [
				Vector({
					interpolation: 'exponential',
					interpolation_speed: 1000,
					representation: {
						f:'probability',
						h: [40, 40, 40, 1],
						interpolation_speed: 500
						},
					v: [155, 255, 255, 10]
				}),Vector({
					interpolation_speed: 500,
					representation: {
						f:'probability',
						h: [40, 40, 40, 2],
						h_speed: 500,
						interpolation_speed: 800
						},
					v: [255, 0, 0, 40]
				}), Vector({
					v: [255, 255, 0, 80],
					representation: {
						interpolation: 'direct'
					}
				})
			],
			vector_interpolation: 'discrete'
		},
	deck_vector = $('#vector-deck'),
	vector_template = $('.vector-template').html(),

	sk = io()

	$('#deck-cfg-submit').click(function() {
		update_model(Number( $('#deck-cfg-vectors').val() ))
		update_deck()
		console.log(model)
	})

	/**
	agrega vectores si no estan, y actualiza model.vectors_l
	*/
	function update_model(vectors_l) {
		if(!vectors_l) vectors_l = model.vectors_l
		while(model.vectors.length < vectors_l) {
			model.vectors.push(Vector())
		}
		model.vectors_l = vectors_l
	}

	/**
	usa la información del modelo para actualizar el tablero
	ejecutada cuando se cambia el numero de vectores de clusterización
	deja conectadas funciones que actualizan el modelo en forma de eventos
	*/
	function update_deck() {
		deck_vector.empty()
		var i, l = model.vectors_l,
			update

		update = function(v, force) {
			if(v.sync) update_brain()
			else if(force) {
				v.sync = true
				update_brain()
				v.sync = false
			}
		}

		for(i=0; i<l; i++) (function() {
			var f, v, h, entry, el
			v = model.vectors[i]
			h = v.representation.h
			deck_vector.append(vector_template
				.replace('__id',i)
				.replace('$inter_speed',v.interpolation_speed || 1)
				.replace('$hzvalue',''+v.v[3])
				.replace('$p_interpolation_speed', v.representation.interpolation_speed)
			)
			entry = $('#vector-entry-'+i)

			$(".vector-color-picker", entry).spectrum({
				flat: true,
				color: tinycolor({r: v.v[0], g:v.v[1], b:v.v[2]}),
				move: function(c) {
					v.v[0] = c._r
					v.v[1] = c._g
					v.v[2] = c._b
					update(v)
				}
			})

			$(".vector-probability-picker", entry).spectrum({
				flat: true,
				color: tinycolor({r: h[0], g:h[1], b:h[2]}),
				move: function(c) {
					v.representation.h[0] = c._r
					v.representation.h[1] = c._g
					v.representation.h[2] = c._b
					update(v)
				}
			})

			//probability handling

			el = $('.vector-probability',entry)
			el.change(function(e) {
				v.representation.f = (e.target.checked)?'probability':'direct'
				update(v)
			})
			el[0].checked = v.representation.f == 'probability'

			el = $('.vector-probability-h-speed',entry)
			el[0].addEventListener('input', function(e) {
				v.representation.h_speed = Number(e.target.value)
				update(v)
			})
			el[0].value = v.representation.h_speed

			el = $('.vector-probability-interpolation',entry)
			el.change(function(e) {
				v.representation.interpolation = (e.target.checked)?'exponential':''
				update(v)
			})
			el[0].checked = v.representation.interpolation == 'exponential'

			el = $('.vector-probability-interpolation-speed',entry)
			el[0].addEventListener('input', function(e) {
				v.representation.interpolation_speed = Number(e.target.value)
				update(v)
			})
			el[0].value = v.representation.interpolation_speed

			//interpolation and syncing

			el = $('.vector-interpolation',entry)
			el.change(function(e) {
				v.interpolation = (e.target.checked)? 'exponential':''
				update(v)
			})
			el[0].checked = v.interpolation == 'exponential'

			el = $('.vector-interpolation-speed',entry)
			el[0].addEventListener('input', function(e) {
				v.interpolation_speed = Number(e.target.value)
				update(v)
			})
			el[0].value = v.interpolation_speed

			el = $('.vector-sync',entry)
			el.change(function(e) {
				v.sync=e.target.checked
				update(v)
			})
			el[0].checked = v.sync

			$('.vector-sync-update',entry).click(function() {
				console.log('clicked send button')
				update(v, true)
			})
			$('.vector-sync-recover',entry).click(function() {
				console.log('clicked recover button')
			})
		})()

	}
	show_brain = function() {
		console.log(model)
	}
	/**
	envia al servidor la configuración actual del escritorio
	este usa esta información para actualizar los celulares
	asociados a cada "cluster" usando la libreria socket.io

	puedes entender el mensage como un "evento"
	*/
	update_brain = function update_brain() {
		sk.emit('update brain', JSON.stringify(model))
	}
	update_deck()
	// setInterval(update_brain,100)
})

})()
