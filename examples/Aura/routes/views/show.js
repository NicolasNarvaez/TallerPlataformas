var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	var store = keystone.get("session options").store
	console.log(req.sessionID)
	store.get(req.sessionID, function(err, data) {
		console.log(err, data)
	})
	// Set locals
	locals.section = 'show';

	// Render the view
	view.render('show');

};
